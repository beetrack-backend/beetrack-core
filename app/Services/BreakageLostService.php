<?php

namespace App\Services;

use App\Constants\BreakageLostConstant;
use App\Models\Asset\Asset;
use App\Models\BreakageLost\BreakageLostReport;
use App\Repositories\BreakageLost\BreakageLostAssetRepositoryInterface;
use App\Repositories\BreakageLost\BreakageLostReportRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BreakageLostService extends BaseService
{
    protected $repository;
    protected $assetBreakageLostRepository;

    public function __construct(
        BreakageLostReportRepositoryInterface $repository,
        BreakageLostAssetRepositoryInterface $assetBreakageLostRepository
    )
    {
        $this->repository = $repository;
        $this->assetBreakageLostRepository = $assetBreakageLostRepository;
    }

    public function detail($id) : Model
    {
        return $this->repository->findById(
            $id,
            ['*'],
            ['location', 'department', 'owner', 'assets', 'approvers.department', 'creator.department']
        );
    }

    public function list($params)
    {
        $params['relations'] = ['location', 'department', 'owner', 'creator'];
        return $this->repository->getByFilters($params);
    }

    public function approve($id, $user)
    {
        try {
            DB::beginTransaction();
            /**
             * @var $item BreakageLostReport
             */
            $item = $this->repository->findById($id, ['*'], ['availableApprovers']);
            if ($this->repository->canApprove($item, $user)) {
                $currentLevel = $item->current_approval_level;
                $approvingLevel = $currentLevel + 1;

                // Get approver
                $currentApprover = $this->repository->getApproverInLevel($item, $currentLevel);
                $approvingApprover = $this->repository->getApproverInLevel($item, $approvingLevel);

                // Process approve
                $item->last_approval_user_id = $approvingApprover->id;
                $item->current_approval_level = $approvingApprover->pivot->level;
                // Update pivot data
                $item->availableApprovers()
                    ->syncWithoutDetaching([
                        $approvingApprover->id => [
                            'approved_at' => date('Y-m-d H:i:s')
                        ]
                    ]);

                // Store future data
                $nextApprover = $this->repository->getApproverInLevel($item, $approvingLevel + 1);
                $item->next_approval_user_id = $nextApprover->id;

                // Check is last approver
                if (!empty($currentApprover)) {
                    if ($currentApprover->pivot->is_last) {

                    } else {
                        // Do nothing
                    }
                } else {
                    $item->status = BreakageLostConstant::STATUS_PROCESSING;
                }

                // Get next approver
                $item->save();
            }

            DB::commit();
            return $item;
        } catch (Exception $e) {
            DB::rollBack();

            Log::error("Fail at: BreakageLostService - approve");
            Log::error($e);

            throw $e;
        }
    }

    public function store($data) : Model
    {
        try {
            DB::beginTransaction();
            // Store the report
            $item = $this->repository->create([
                'report_user_id' => $data['report_user_id'],
                'owner_id' => $data['owner_id'] ?? null,
                'location_id' => $data['location_id'] ?? null,
                'department_id' => $data['department_id'] ?? null,
                'status' => BreakageLostConstant::STATUS_NEW,
                'current_approval_level' => BreakageLostConstant::LEVEL_DEFAULT,
            ]);

            // Process store breakage lost assets
            if (isset($data['assets'])) {
                $dataAssets = collect($data['assets'])->keyBy('asset_id');
                $assets = Asset::with(['location', 'department', 'owner'])
                    ->whereIn('asset_id', array_column($data['assets'], 'asset_id'))
                    ->get();

                // Add asset breakage lost
                foreach ($assets as $asset)
                {
                    if (empty($dataAssets[$asset->getKey()])) {
                        continue;
                    }

                    $dataAsset = $dataAssets[$asset->getKey()];
                    $this->assetBreakageLostRepository->create([
                        'breakage_lost_report_id' => $item->getKey(),
                        'asset_id' => $asset->getKey(),
                        'asset' => $asset,
                        'quantity' => $dataAsset['quantity'],
                        'location_id' => $asset->location_id,
                        'location' => $asset->location,
                        'department_id' => $asset->department_id,
                        'department' => $asset->department,
                        'caused_by_user_id' => $dataAsset['caused_by_user_id'],
                        'status' => BreakageLostConstant::STATUS_NEW,
                    ]);
                }
            }

            // Process store breakage lost approval users
            if (!empty($data['approval_users'])) {
                $dataUsers = [];
                $user_id = null;
                // Store first approver is the next one
                $item->next_approval_user_id = $data['approval_users'][0];
                $item->save();

                foreach ($data['approval_users'] as $level => $user_id) {
                    $dataUsers[$user_id] = [
                        'is_last' => false,
                        'status' => null,
                        'approved_at' => null,
                        'denied_at' => null,
                        'level' => $level + 1
                    ];
                }
                // Last user will be true
                if (!is_null($user_id)) {
                    $dataUsers[$user_id]['is_last'] = true;
                }
                $item->approvers()->attach($dataUsers);
            }

            DB::commit();

            return $item->fresh();
        } catch (Exception $e) {
            Log::error("Fail at: BreakageLostService - store");
            Log::error($e);

            throw $e;
        }
    }
}
