<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

interface ServiceInterface
{
    public function store($data) : Model;
    public function detail($id) : Model;
}
