<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Resources\BreakageLost\BreakageLostDetailResource;
use App\Resources\BreakageLost\BreakageLostListResource;
use App\Services\BreakageLostService;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class BreakageLostApiController extends ApiController
{
    protected $service;

    public function __construct(BreakageLostService $service)
    {
        parent::__construct();
        $this->service = $service;
        $this->guard = 'internal_service';
    }

    public function store(Request $request)
    {
        try {
            $validated = $this->validate($request, [
                'location_id' => 'required|exists:locations,id',
                'department_id' => 'nullable|exists:departments,id',
                'owner_id' => 'nullable|exists:owners,id',
                'approval_users' => 'required|array',
                'approval_users.*' => 'integer',
                'assets' => 'required|array',
                'assets.*.asset_id' => 'integer|min:0',
                'assets.*.quantity' => 'integer|min:0',
                'assets.*.caused_by_user_id' => 'integer|min:0',
            ]);

            $validated['report_user_id'] = $request->user($this->guard)->getKey();
            return $this->sendResponse(
                BreakageLostDetailResource::make($this->service->store($validated))
            );
        } catch (Exception $e) {
            Log::error('BreakageLostApiController - store');
            Log::error($e);
            if ($e instanceof ValidationException) {
                return $this->sendError(400, $e->getMessage(), $e->errors());
            }

            return $this->sendError(500, $e->getMessage());
        }
    }

    public function list(Request $request)
    {
        try {
            $validated = $this->validate($request, [
                'location_id' => 'nullable|exists:locations,id',
                'department_id' => 'nullable|exists:departments,id',
                'owner_id' => 'nullable|exists:owners,id',
                'page' => 'required|integer',
                'limit' => 'required|integer',
                'order_by' => 'nullable',
                'order_direction' => 'nullable|in:asc,desc'
            ]);
            return $this->sendResponse(
                BreakageLostListResource::collection($this->service->list($validated))
            );
        } catch (Exception $e) {
            Log::error('BreakageLostApiController - list');
            Log::error($e);
            if ($e instanceof ValidationException) {
                return $this->sendError(400, $e->getMessage(), $e->errors());
            }

            return $this->sendError(500, $e->getMessage());
        }
    }

    public function detail($id)
    {
        try {
            return $this->sendResponse(
                BreakageLostDetailResource::make($this->service->detail($id))
            );
        } catch (Exception $e) {
            Log::error('BreakageLostApiController - detail');
            Log::error($e);

            return $this->sendError(500, $e->getMessage());
        }
    }

    public function approve($id, Request $request)
    {
        try {
            return $this->sendResponse(
                BreakageLostDetailResource::make($this->service->approve($id, $request->user($this->guard)))
            );
        } catch (Exception $e) {
            Log::error('BreakageLostApiController - detail');
            Log::error($e);

            return $this->sendError(500, $e->getMessage());
        }
    }
}
