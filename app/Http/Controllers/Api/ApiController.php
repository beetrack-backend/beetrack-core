<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    private $_errors = [];
    private $_debugSql;
    private $_debug;
    private $_fields;
    public $guard;

    public function __construct()
    {
        $this->_debug = false;
        if (config('app.debug')) {
            $this->_debug = true;

            DB::listen(function ($sql) {
                $this->_debugSql[uniqid()] = $sql;
            });
        }
    }

    /**
     * @param $code
     *
     * @return $this
     */
    protected function setCode($code)
    {
        $this->_code = $code;

        return $this;
    }

    /**
     * @param string|int $appCode
     *
     * @return $this
     */
    protected function setAppCode($appCode)
    {
        $this->_appCode = $appCode;

        return $this;
    }

    /**
     * @param $status
     *
     * @return $this
     */
    protected function setStatus($status)
    {
        $this->_status = $status;

        return $this;
    }

    /**
     * @param $message
     *
     * @return $this
     */
    protected function setMessage($message)
    {
        $this->_message = $message;

        return $this;
    }

    /**
     * @param $data
     *
     * @return $this
     */
    protected function setData($data)
    {
        $this->_data = $data;

        return $this;
    }

    /***
     * @param $name
     * @param $value
     *
     * @return $this
     */
    protected function addField($name, $value)
    {
        $this->_fields[$name] = $value;

        return $this;
    }

    /**
     * @param $error_name
     * @param $msg
     *
     * @return $this
     */
    protected function addError($error_name, $msg = '')
    {
        $this->_errors[$error_name] = $msg;

        return $this;
    }

    /**
     * @param $errors
     * @return $this
     */
    protected function addErrors($errors)
    {
        $this->_errors = array_merge($this->_errors, $errors);
        return $this;
    }

    /**
     * success response method.
     *
     * @param $data
     * @param string $message
     *
     * @return JsonResponse
     */
    public function sendResponse($data, string $message = 'Successfully'): JsonResponse
    {
        $response = [
            'success' => true,
            'data' => $data,
            'message' => $message,
            'errors' => [],
            'errorCode' => null,
        ];
        $response = $this->getDebugInfo($response);

        return response()->json($response);
    }

    /**
     * return error response.
     *
     * @param  $errorCode
     * @param string $errorMessages
     * @param array $errors
     * @param int $httpStatus
     *
     * @return JsonResponse
     */
    public function sendError($errorCode, string $errorMessages, array $errors = [], int $httpStatus = 404): JsonResponse
    {
        $response = [
            'success' => false,
            'data' => null,
            'message' => $errorMessages,
            'errors' => $errors,
            'errorCode' => $errorCode,
        ];
        $response = $this->getDebugInfo($response);

        return response()->json($response, $httpStatus);
    }

    public function getDebugInfo($data)
    {
        // debug mode
        if ($this->_debug !== false) {
            $_total_queries = $this->_debugSql ? count($this->_debugSql) : 0;
            $execute_time = microtime(true) - LUMEN_START;
            $this->addField('__EXECUTE_TIME', $execute_time);
            $this->addField('__TOTAL_QUERIES', $_total_queries);
            $this->addField('__debug_sql', $this->_debugSql);

            $data['debug'] = $this->_fields;
        }

        return $data;
    }
}
