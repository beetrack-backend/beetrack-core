<?php

namespace App\Resources\Asset;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BasicAssetInfoResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'asset_id' => $this->asset_id,
            'asset_code' => $this->asset_code,
            'barcode' => $this->barcode,
            'name' => $this->name,
            'alternative_name' => $this->alternative_name,
            'description' => $this->description,
            'image_url' => $this->image_url,
            'price' => $this->price,
            'book_value' => $this->book_value,
        ];
    }
}
