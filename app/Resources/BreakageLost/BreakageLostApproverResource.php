<?php

namespace App\Resources\BreakageLost;

use App\Resources\Asset\BasicAssetInfoResource;
use App\Resources\Department\DepartmentBasicInfoResource;
use App\Resources\Location\LocationBasicInfoResource;
use App\Utilities\FileHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BreakageLostApproverResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'email' => $this->email,
            'job_title' => $this->job_title,
            'pivot' => $this->pivot,
            'department' => DepartmentBasicInfoResource::make($this->whenLoaded('department')),
            'avatar' => !empty($this->avatar) ? FileHelper::getFileUrl($this->avatar) : null,
        ];
    }
}
