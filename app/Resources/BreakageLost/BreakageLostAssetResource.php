<?php

namespace App\Resources\BreakageLost;

use App\Resources\Asset\BasicAssetInfoResource;
use App\Resources\Department\DepartmentBasicInfoResource;
use App\Resources\Location\LocationBasicInfoResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BreakageLostAssetResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'breakage_lost_report_id' => $this->breakage_lost_report_id,
            'asset_id' => $this->asset_id,
            'asset' => !empty($this->asset) ? BasicAssetInfoResource::make((object) $this->asset) : null,
            'quantity' => $this->quantity,
            'location_id' => $this->location_id,
            'location' => !empty($this->location) ? LocationBasicInfoResource::make((object) $this->location) : null,
            'department_id' => $this->department_id,
            'department' => !empty($this->department) ? DepartmentBasicInfoResource::make((object) $this->department) : null,
            'caused_by_user_id' => $this->caused_by_user_id,
            'status' => $this->status,
        ];
    }
}
