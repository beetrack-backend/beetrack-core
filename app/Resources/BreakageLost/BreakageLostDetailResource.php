<?php

namespace App\Resources\BreakageLost;

use App\Resources\Department\DepartmentBasicInfoResource;
use App\Resources\Location\LocationBasicInfoResource;
use App\Resources\Owner\OwnerBasicInfoResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BreakageLostDetailResource extends BreakageLostListResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $listData = parent::toArray($request);
        return array_merge($listData, [
            'assets' => BreakageLostAssetResource::collection($this->whenLoaded('assets')),
            'approvers' => BreakageLostApproverResource::collection($this->whenLoaded('approvers')),
        ]);
    }
}
