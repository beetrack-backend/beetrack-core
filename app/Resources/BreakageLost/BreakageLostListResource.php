<?php

namespace App\Resources\BreakageLost;

use App\Resources\Department\DepartmentBasicInfoResource;
use App\Resources\Location\LocationBasicInfoResource;
use App\Resources\Owner\OwnerBasicInfoResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BreakageLostListResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'location_id' => $this->location_id,
            'location' => LocationBasicInfoResource::make($this->whenLoaded('location')),
            'department_id' => $this->department_id,
            'department' => DepartmentBasicInfoResource::make($this->whenLoaded('department')),
            'owner_id' => $this->owner_id,
            'owner' => OwnerBasicInfoResource::make($this->whenLoaded('owner')),
            'assets_count' => $this->assets_count,
            'last_approval_user_id' => $this->last_approval_user_id,
            'next_approval_user_id' => $this->next_approval_user_id,
            'current_approval_level' => $this->current_approval_level,
            'creator' => BreakageLostApproverResource::make($this->whenLoaded('creator')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
