<?php

namespace App\Resources\Location;

use App\Resources\Common\ItemBasicInfoResource;
use Illuminate\Http\Request;

class LocationBasicInfoResource extends ItemBasicInfoResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
