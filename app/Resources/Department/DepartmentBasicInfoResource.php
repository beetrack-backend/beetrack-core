<?php

namespace App\Resources\Department;

use App\Resources\Common\ItemBasicInfoResource;
use Illuminate\Http\Request;

class DepartmentBasicInfoResource extends ItemBasicInfoResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
