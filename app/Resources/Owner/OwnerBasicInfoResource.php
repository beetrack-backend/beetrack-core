<?php

namespace App\Resources\Owner;

use App\Resources\Common\ItemBasicInfoResource;
use Illuminate\Http\Request;

class OwnerBasicInfoResource extends ItemBasicInfoResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
