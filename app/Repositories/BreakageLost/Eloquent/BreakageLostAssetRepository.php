<?php

namespace App\Repositories\BreakageLost\Eloquent;

use App\Models\BreakageLost\BreakageLostAsset;
use App\Repositories\BreakageLost\BreakageLostAssetRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class BreakageLostAssetRepository extends BaseRepository implements BreakageLostAssetRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @param BreakageLostAsset $model
     */
    public function __construct(BreakageLostAsset $model)
    {
        $this->model = $model;
    }


    /**
     * Get by filters.
     *
     * @param array $filters
     * @return LengthAwarePaginator
     */
    public function getByFilters(array $filters): LengthAwarePaginator
    {
        $query = $this->model;
        if (isset($filters['owner_id'])) {
            $query->where('owner_id', $filters['owner_id']);
        }
        if (isset($filters['location_id'])) {
            $query->where('location_id', $filters['location_id']);
        }
        if (isset($filters['location_id'])) {

        }
        return $this->model->paginate();
    }
}
