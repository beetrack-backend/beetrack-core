<?php

namespace App\Repositories\BreakageLost\Eloquent;

use App\Models\BreakageLost\BreakageLostReport;
use App\Repositories\BreakageLost\BreakageLostReportRepositoryInterface;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Exception;
use Illuminate\Support\Collection;

class BreakageLostReportRepository extends BaseRepository implements BreakageLostReportRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @param BreakageLostReport $model
     */
    public function __construct(BreakageLostReport $model)
    {
        $this->model = $model;
    }

    public function getByFilters(array $filters): LengthAwarePaginator
    {
        $query = $this->model->withCount('assets');
        if (!empty($filters['relations'])) {
            $query->with($filters['relations']);
        }
        if (!empty($filters['location_id'])) {
            $query->where('location_id', $filters['location_id']);
        }
        if (!empty($filters['department_id'])) {
            $query->where('department_id', $filters['department_id']);
        }
        if (!empty($filters['owner_id'])) {
            $query->where('owner_id', $filters['owner_id']);
        }
        if (!empty($filters['order_by']) && !empty($filters['order_direction'])) {
            $query->orderBy($filters['order_by'], $filters['order_direction']);
        }

        $query->forPage($filters['page'], $filters['limit']);
        return $query->paginate();
    }

    public function canApprove(Model $item, Authenticatable $user): bool
    {
        /**
         * @var $approvers Collection
         */
        $approvers = $item->availableApprovers;
        if (empty($approvers)) {
            throw new Exception('There is no approvers for approval');
        }

        // Get next approver but next is not next actually
        $approver = $approvers->first();
        if ($approver->pivot->level !== $item->current_approval_level + 1) {
            throw new Exception('Conflict information. Please check again');
        }

        if ($approver->id !== $user->getAuthIdentifier()) {
            throw new Exception('Sorry. You are not the next approver of this report');
        }

        return true;
    }

    public function getApproverInLevel(Model $item, $level): ?Model
    {
        return $item->availableApprovers->firstWhere('pivot.level', $level);
    }

    public function applyReport(Model $item) : void
    {
//        $reportAssets = $item->asset
    }
}
