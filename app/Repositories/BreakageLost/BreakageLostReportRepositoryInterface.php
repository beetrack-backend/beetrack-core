<?php

namespace App\Repositories\BreakageLost;

use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

interface BreakageLostReportRepositoryInterface extends EloquentRepositoryInterface
{
    public function canApprove(Model $item, Authenticatable $user): bool;
    public function getApproverInLevel(Model $item, $level) : ?Model;
    public function applyReport(Model $item) : void;
}
