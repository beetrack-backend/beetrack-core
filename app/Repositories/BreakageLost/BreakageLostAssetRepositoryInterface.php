<?php

namespace App\Repositories\BreakageLost;

use App\Repositories\EloquentRepositoryInterface;

interface BreakageLostAssetRepositoryInterface extends EloquentRepositoryInterface{}
