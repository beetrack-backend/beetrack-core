<?php

namespace App\Libraries\Guards;

use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;

class RequestTokenGuard implements Guard
{
    use GuardHelpers;

    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    public function user()
    {
        if ($this->user)
            return $this->user;

        $user = null;
        $token = request()->server()['USER-ID'];

        if ($token) {
            $user = $this->provider->retrieveById($token);
        }

        return $this->user = $user;
    }

    public function validate(array $credentials = [])
    {

    }
}
