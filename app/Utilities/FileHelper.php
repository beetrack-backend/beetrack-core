<?php

namespace App\Utilities;

use Exception;
use Illuminate\Support\Facades\Log;

class FileHelper
{
    public static function composeFileData($files, $storage = null, $filePath = false)
    {
        if (is_null($files)) {
            return [
                [
                    'file' => null,
                    'url' => null,
                ],
            ];
        }

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' && !$filePath) {
            // windows, file path use \, we need to switch to /
            return array_map(function ($file) use ($storage) {
                $file_info = array_values($file)[0];
                return array(
                    'file' => $file_info['file'],
                    'url' => str_replace('\\', '/', $file_info['path']),
                );
            }, $files);
        } else {
            // linux, or windows with real path file (use \)
            return array_map(function ($file) use ($storage) {
                $file_info = array_values($file)[0];
                return array(
                    'file' => $file_info['file'],
                    'url' => $file_info['path'],
                );
            }, $files);
        }
    }

    public static function getFileUrl($files, $storage = null, $filePath = false)
    {
        if (is_null($files)) {
            return null;
        }
        if (empty($files)) {
            return "";
        }
        $url = '';
        try {
            $ret = self::composeFileData($files, $storage, $filePath);
            if (is_array($ret)) {
                $url = $ret[0]['url'];
            } else {
                $url = $files;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
        return $url;
    }
}
