<?php

namespace App\Constants;

final class BreakageLostConstant
{
    public const STATUS_NEW = 1;
    public const STATUS_PROCESSING = 2;

    public const LEVEL_DEFAULT = 0;
}
