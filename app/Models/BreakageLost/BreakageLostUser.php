<?php

namespace App\Models\BreakageLost;

use Illuminate\Database\Eloquent\Model;

class BreakageLostUser extends Model
{
    protected $table = 'breakage_lost_users';
}
