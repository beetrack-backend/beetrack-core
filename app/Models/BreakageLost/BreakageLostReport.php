<?php

namespace App\Models\BreakageLost;

use Illuminate\Database\Eloquent\Model;

class BreakageLostReport extends Model
{
    use BreakageLostReportRelations;

    protected $table = 'breakage_lost_reports';

    protected $fillable = [
        'report_user_id',
        'owner_id',
        'location_id',
        'department_id',
        'status',
        'current_approval_level',
    ];
}
