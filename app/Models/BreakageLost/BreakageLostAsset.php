<?php

namespace App\Models\BreakageLost;

use Illuminate\Database\Eloquent\Model;

class BreakageLostAsset extends Model
{
    protected $table = 'breakage_lost_assets';

    protected $fillable = [
        'breakage_lost_report_id',
        'asset_id',
        'asset',
        'quantity',
        'location_id',
        'location',
        'department_id',
        'department',
        'caused_by_user_id',
        'status'
    ];

    protected $casts = [
        'asset' => 'array',
        'location' => 'array',
        'department' => 'array',
    ];
}
