<?php

namespace App\Models\BreakageLost;

use Illuminate\Support\Facades\DB;

trait BreakageLostReportRelations
{
    public function assets()
    {
        return $this->hasMany('App\Models\BreakageLost\BreakageLostAsset', 'breakage_lost_report_id');
    }

    public function approvers()
    {
        return $this->belongsToMany(
            'App\Models\User',
            'breakage_lost_users',
            'report_id',
            'user_id'
        )->withPivot([
            'level',
            'is_last',
            'status',
            'approved_at',
            'denied_at',
        ]);
    }

    public function availableApprovers()
    {
        $blUserTable = (new BreakageLostUser())->getTable();
        return $this->approvers()->whereNull(DB::raw($blUserTable . '.approved_at'));
    }

    public function breakageLostUsers()
    {
        return $this->hasMany('App\Models\BreakageLost\BreakageLostUser', 'report_id');
    }

    // Approves who not yet take any actions
    public function availableBreakageLostUsers()
    {
        return $this->breakageLostUsers()->whereNull('approved_at')->get();
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location\Location', 'location_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department\Department', 'department_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\Owner\Owner', 'owner_id');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'report_user_id');
    }
}
