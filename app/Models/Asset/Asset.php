<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use AssetRelations;
    protected $table = 'asset';
    protected $primaryKey = 'asset_id';
}
