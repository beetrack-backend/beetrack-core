<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class AssetView extends Model
{
    protected $table = 'asset_view';
}
