<?php

namespace App\Models\Asset;

trait AssetRelations
{
    public function location()
    {
        return $this->belongsTo('App\Models\Location\Location', 'location_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department\Department', 'department_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\Owner\Owner', 'owner_id');
    }
}
