<?php

namespace App\Providers;

use App\Libraries\Guards\RequestTokenGuard;
use App\Libraries\Providers\AuthInternalServiceProvider;
use App\Models\User;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
        });

        $this->app['auth']->provider('internal_request', function() {
            return new AuthInternalServiceProvider();
        });

        $this->app['auth']->extend('request_token', function($app, $name, array $config) {
            return new RequestTokenGuard($this->app['auth']->createUserProvider($config['provider']));
        });
    }
}
