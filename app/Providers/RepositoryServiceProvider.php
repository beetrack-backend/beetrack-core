<?php

namespace App\Providers;

use App\Repositories\BreakageLost\BreakageLostAssetRepositoryInterface;
use App\Repositories\BreakageLost\BreakageLostReportRepositoryInterface;
use App\Repositories\BreakageLost\Eloquent\BreakageLostAssetRepository;
use App\Repositories\BreakageLost\Eloquent\BreakageLostReportRepository;
use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);

        /**
         * Breakage & Lost
         */
        $this->app->bind(BreakageLostReportRepositoryInterface::class, BreakageLostReportRepository::class);
        $this->app->bind(BreakageLostAssetRepositoryInterface::class, BreakageLostAssetRepository::class);
    }
}
