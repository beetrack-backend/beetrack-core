<?php

return [
    'defaults' => [
        'guard' => env('AUTH_GUARD', 'api'),
    ],
    'guards' => [
        'api' => ['driver' => 'api'],
        'internal_service' => [
            'driver' => 'request_token',
            'provider' => 'internal_service',
        ]
    ],
    'providers' => [
        'internal_service' => [
            'driver' => 'internal_request',
        ],
    ],
];
