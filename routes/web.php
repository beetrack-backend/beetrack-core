<?php

/** @var Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/be/health', ['as' => 'heath', function () use ($router) {
    try {
        app('db')->connection()->getPdo();

        return response([
            'success' => true
        ]);
    } catch (Exception $e) {
        return response([
            'success' => false
        ], 500);
    }
}]);

$router->post('/be/health', function () use ($router) {
    return $router->app->version();
});


/**
 * V2
 */
$router->group(
    [
        'prefix' => 'be'
    ],
    function () use ($router) {
        $router->group(
            [
                'prefix' => 'api/v1',
            ],
            function () use ($router) {
                require 'v1/api.php';
            }
        );
    }
);
