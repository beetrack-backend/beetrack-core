<?php

/** @var Laravel\Lumen\Routing\Router $router */

$router->group(
    [
        'middleware' => 'auth:internal_service'
    ],
    function () use ($router) {
        /**
         * Breakage and Lost
         */
        $router->group(
            [
                'prefix' => 'breakage-lost'
            ],
            function () use ($router) {
                $router->put('approve/{id}', 'Api\V1\BreakageLostApiController@approve');
                $router->get('/{id}', 'Api\V1\BreakageLostApiController@detail');
                $router->get('/', 'Api\V1\BreakageLostApiController@list');
                $router->post('/', 'Api\V1\BreakageLostApiController@store');
            }
        );
    }
);
